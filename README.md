# Slide Template Builder Docker Image

For [Beamer Slide Template][beamer-slide-template] project

## Installation

```bash
docker login registry.gitlab.com
docker pull registry.gitlab.com:/ipl-uec/docker-slidetemp-builder
```

[beamer-slide-template]: https://gitlab.com/ipl-uec/beamer-slide-template
