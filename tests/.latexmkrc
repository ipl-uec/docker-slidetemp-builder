#!/usr/bin/env perl

# tex options
$latex        = 'platex -synctex=1 --shell-escape -halt-on-error';
$latex_silent = $latex . ' -interaction=batchmode';
$biber        = 'biber %O --bblencoding=utf8 -u -U --output_safechars %B';
$bibtex       = 'pbibtex %O %B';
$dvipdf       = 'dvipdfmx %O -o %D %S';
$makeindex    = 'mendex %O -o %D %S';
$max_repeat   = 5;
$pdf_modei    = 3;

