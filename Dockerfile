FROM registry.gitlab.com/ipl-uec/docker-platex:latest

RUN apt-get update \
 && apt-get install -y \
    make \
 && rm -rf /var/lib/apt/*

RUN tlmgr install latexmk

CMD ["make"]

